import React, { Component } from "react";
import "./Test.css";

class TestState extends Component {
  state = {
    visible: true,
    title: "hide"
  };

  onHandle = () => {
    this.state.title === "show"
      ? this.setState({ visible: true, title: "hide" })
      : this.setState({ visible: false, title: "show" });
  };

  render() {
    return (
      <div>
        <div style={{ height: "30px", width: "100px" }}>
          {this.state.visible ? <p>Hello john</p> : null}
        </div>
        <div style={{ display: "flex" }}>
          <button onClick={this.onHandle}>{this.state.title}</button>
        </div>
      </div>
    );
  }
}

export default TestState;
