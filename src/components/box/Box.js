import React from "react";

import "./box.css";

class Box extends React.Component {
  state = {
    todo: "",
    todos: []
  };

  ids = () => {
    return new Date() + this.todos.length + 1;
  };

  handleTodo = event => {
    this.setState({ todo: event.target.value });
    //       let newSelected = _.extend({}, this.state.todo);
    // newSelected.id = this.ids();
    // this.setState({ todo: newSelected });
  };

  onDelete = id => {};

  addTodo = () => {
    this.setState({ todos: [...this.state.todos, this.state.todo] });
  };

  render() {
    return (
      <div className="all">
        <div className="topDiv">
          <div className="inputDiv">
            <input onChange={this.handleTodo} />
          </div>
          <div className="btnDiv" onClick={this.addTodo}>
            <p>add</p>
          </div>
        </div>
        <div className="downDiv">
          <div className="todoDiv">
            {this.state.todos.map(e => (
              <div className="todo_row">
                <p className="todo">{e}</p>
                <p className="delete">X</p>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default Box;
