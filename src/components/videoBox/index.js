/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import "./videobox.css";

class VideoBox extends React.Component {
  render() {
    return (
      <div style={{ width: this.props.width1, padding: "10px" }}>
        <div>
          <img
            src={this.props.img1}
            width="100%"
            style={{ borderRadius: "5px" }}
          />
        </div>
        <div className="video-title">
          <p>{this.props.title}</p>
        </div>
        <div className="video-desc">
          <p>{this.props.desc}</p>
        </div>
      </div>
    );
  }
}

export default VideoBox;
