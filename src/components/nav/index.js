import React from "react";
import "./index.css";
import Contents from "../content/";

class Navigation extends React.Component {
  render() {
    return (
      <div className="all">
        <div className="logo">Logo Here</div>
        <div>
          <Contents content="about us" />
        </div>
        <div className="navbar">
          <p>{this.props.name}</p>
          <p>{this.props.another}</p>
          <p>{this.props.anotherone}</p>
        </div>
      </div>
    );
  }
}

export default Navigation;
