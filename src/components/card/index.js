import React from "react";
import { Card } from "antd";

class Cards extends React.Component {
  array = ["name", "name2", "name3", "name4", "name5", "name6"];

  student1 = {
    name: "majed",
    major: "software engineering",
    age: 89,
    id: 123456
  };
  student2 = {
    ...this.student1,
    age: 90,
    aga: 76,
    id: 676475746
  };

  render() {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        {this.array.map(element => (
          <p>{element}</p>
        ))}

        <p>{JSON.stringify(this.student1)}</p>
        <p>{JSON.stringify(this.student2)}</p>

        <Card
          title="Default size card"
          extra={<a href="#">More</a>}
          style={{ width: 300 }}
        >
          <p>Card content</p>
          <p>Card content</p>
          <p>Card content</p>
        </Card>
      </div>
    );
  }
}

export default Cards;
